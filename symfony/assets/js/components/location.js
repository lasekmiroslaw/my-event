document.addEventListener('DOMContentLoaded', () => {
    const country = document.getElementById('countries');
    country.param = 'regions';

    country.onchange = onchangeData

    function onchangeData(event) {
        const element = event.target;
        const dataString = element.param;

        const data = {};
        data[element.getAttribute('name')] = element.value;

        const encodedData = urlEncodeData(data);

        fetchData(encodedData, dataString)
            .then(() => {
                const region = document.getElementById('regions');
                region.param = 'cities';
                region.onchange = onchangeData;
            })
    }

    function urlEncodeData(data) {
        return  Object.keys(data).map((key) => {
            return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
        }).join('&');
    }

    function fetchData(encodedData, dataString) {
        const form = document.querySelector('form');

        return fetch(window.location.href, {
            method: form.getAttribute('method'),
            credentials: "same-origin",
            headers: {
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            },
            body: encodedData
        })
            .then((response) => {
                return response.text();
            })
            .then((html) => {
                clearCityList();

                const oldValue = document.getElementById(dataString);
                const parsed = document.createRange().createContextualFragment(html);
                const newValue = parsed.querySelector(`#${dataString}`);
                oldValue.parentElement.replaceWith(newValue);

                M.FormSelect.init(document.getElementById(dataString));
            })
            .catch((err) => {
                console.log('Request failed', err);
            });
    }

    function clearCityList() {
        const cities = document.getElementById('cities');
        const citiesUl = document.getElementById('cities').previousSibling.previousSibling;

        cities.value = 'Your city';
        citiesUl.remove();
    }
})