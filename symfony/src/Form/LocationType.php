<?php

namespace App\Form;

use App\Entity\Country;
use App\Entity\Location;
use App\Entity\Region;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocationType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('country', EntityType::class, array(
                'class' => 'App\Entity\Country',
                'placeholder' => 'Your country',
                'choice_label' => 'name',
            ));

        $this->countryAjaxFieldFormListener($builder);
        $this->regionAjaxFieldFormListener($builder);
    }

    private function countryAjaxFieldFormListener(FormBuilderInterface $builder)
    {
        $localizationFormModifier = function (FormInterface $form, Country $country = null) {
            $regions = null === $country ? array() : $country->getRegions();

            if ($form->has('region') && !$form->isSubmitted()) {
                $form->remove('region');
            }

            $form->add('region', EntityType::class, array(
                'class' => 'App\Entity\Region',
                'placeholder' => 'Your region',
                'choices' => $regions,
                'choice_label' => 'name'
            ));
        };

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($localizationFormModifier) {
            /** @var Location $data */
            $data = $event->getData();
            $country = null !== $data ? $data->getCountry() : null;

            $localizationFormModifier($event->getForm(), $country);
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($localizationFormModifier) {
            $data = $event->getData();

            $countryId = array_key_exists('country', $data) ? $data['country'] : null;
            if ($countryId !== null) {
                $country = $this->entityManager->getRepository(Country::class)->find($countryId);
                $localizationFormModifier($event->getForm(), $country);
            }
        });
    }

    private function regionAjaxFieldFormListener(FormBuilderInterface $builder)
    {
        $localizationFormModifier = function (FormInterface $form, Region $region = null) {
            $cities = null === $region ? array() : $region->getCities();

            if ($form->has('city') && !$form->isSubmitted()) {
                $form->remove('city');
            }

            $form->add('city', EntityType::class, array(
                'class' => 'App\Entity\City',
                'placeholder' => 'Your city',
                'choices' => $cities,
                'choice_label' => 'name'
            ));
        };

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($localizationFormModifier) {
            /** @var Location $data */
            $data = $event->getData();
            $region = null !== $data ? $data->getRegion() : null;

            $localizationFormModifier($event->getForm(), $region);
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($localizationFormModifier) {
            $data = $event->getData();

            $regionId = array_key_exists('region', $data) ? $data['region'] : null;

            if ($regionId !== null) {
                $region = $this->entityManager->getRepository(Region::class)->find($regionId);
                $localizationFormModifier($event->getForm(), $region);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Location::class
        ]);
    }
}