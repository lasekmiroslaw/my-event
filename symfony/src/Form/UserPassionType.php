<?php

namespace App\Form;

use App\Entity\Passion;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\AssetsHelper;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserPassionType extends AbstractType
{
    /**
     * @var AssetsHelper
     */
    private $assetsHelper;

    public function __construct(Packages $assetsHelper)
    {
        $this->assetsHelper = $assetsHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('passions', EntityType::class, array(
                'class' => Passion::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => function ($passion) {
                    return '<div class="col s3 pull-s2"><span class="flow-text">' . $passion->getName() . '</span></div>' . ' 
                    <div class="col s3 pull-s4"><img src="' .
                        $this->assetsHelper->getUrl('build/img/passion/' . $passion->getImage()) . '" alt="' . $passion->getName() . '"
                        class="responsive-img" height="128" width="128">
                     </div>';
                },
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}