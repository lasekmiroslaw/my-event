<?php

namespace App\Controller;

use App\Entity\Passion;
use App\Form\PassionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PassionController extends AbstractController
{
    /**
     * @Route("/register/add/passion", name="post_passion")
     */
    public function postPassion(Request $request)
    {
        $passion = new Passion();

        $form = $this->createForm(
            PassionType::class,
            $passion
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()
                ->getManager();

            $entityManager->persist($passion);
            $entityManager->flush();
        }

        return $this->render(
            'passion/post_passion.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}
