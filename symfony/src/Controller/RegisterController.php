<?php

namespace App\Controller;

use App\Entity\Location;
use App\Entity\User;
use App\Form\LocationType;
use App\Form\UserPassionType;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(SessionInterface $session, EntityManagerInterface $entityManager)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/register", name="user_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();

        $form = $this->createForm(
            UserType::class,
            $user
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->session->set('userId', $user->getId());

            return $this->redirectToRoute('user_location');
        }

        return $this->render(
            'register/register.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/register/location", name="user_location")
     */
    public function location(Request $request)
    {
        if (!$this->session->has('userId')) {
            return $this->redirectToRoute('user_register');
        } else {
            $userId = $this->session->get('userId');
        }

        $location = new Location();

        $form = $this->createForm(
            LocationType::class,
            $location
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $user User */
            $user = $this->entityManager->getRepository(User::class)->find($userId);
            $user->setLocation($location);

            $this->entityManager->persist($location);
            $this->entityManager->flush();

            return $this->redirectToRoute('user_passion');
        }

        return $this->render(
            'register/location.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @Route("/register/passion", name="user_passion")
     */
    public function passion(Request $request)
    {
        if (!$this->session->has('userId')) {
            return $this->redirectToRoute('user_register');
        } else {
            $userId = $this->session->get('userId');
        }

        $user = $this->entityManager->getRepository(User::class)->find($userId);

        $form = $this->createForm(
            UserPassionType::class,
            $user
        );
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render(
            'register/passsion.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }
}